jQuery(document).ready(function($) {

  //menu toggle
  $('.nav-toggle').click(function(e) {
    e.preventDefault();
    $('.nav--mobile').toggleClass('is-open');
    $('body').toggleClass('is-fixed');
  });


  //owl slider/carousel
  var owl = $('.slider__track');

  owl.owlCarousel({
      loop: true,
      items: 1,
      autoplay: true,
      // nav: true,
      autplaySpeed: 11000,
      autoplayTimeout: 10000,
      smartSpeed: 250,
      smartSpeed: 2200,
      navSpeed: 2200
      // navText : ["<i class='fa fa-arrow-left' aria-hidden='true'></i>", "<i class='fa fa-arrow-right' aria-hidden='true'></i>"]
  });

  //owl slider/carousel
  var owl = $('.slider__track--references');

  owl.owlCarousel({
      loop: true,
      items: 3,
      autoplay: true,
      nav: true,
      autplaySpeed: 11000,
      autoplayTimeout: 10000,
      smartSpeed: 250,
      smartSpeed: 2200,
      navSpeed: 2200,
      navText : ["<i class='fa fa-arrow-left' aria-hidden='true'></i>", "<i class='fa fa-arrow-right' aria-hidden='true'></i>"]
  });

  //AOS initialize
  AOS.init({
    duration: 800,
    once: true,
    easing: "ease"
  })

  //delay link-boxes animation
  $('.link-boxes__item').each(function(index){
      var delayNumber = index * 100;
      $(this).attr('data-aos-delay', delayNumber).data('delayNumber');
  });

  //sticky header on desktop
  (function() { 
    var $header = $('.header');
    var $offset = $('main');
    var $headerHeight = $header.outerHeight();

      $(window).on('load resize scroll', function(e) {
        var scrollTop = $(window).scrollTop();

          if ( scrollTop >= 1 ) {
            $header.addClass('is-sticky');

            if ($(window).width() > 768) {
              $offset.css("margin-top", "113px");
            }
          } 

          else {
            $header.removeClass('is-sticky');

            if ($(window).width() > 768) {
              $offset.css("margin-top", $headerHeight);
            }
          }

      });

  })();

});
