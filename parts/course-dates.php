<?php 

	//Course repeater field group: Options page
	$content = get_field('course_content', 'options');


	if (have_rows('course', 'options') ) : while (have_rows('course', 'options') ) : the_row();
	$date = get_sub_field('course_date');
	$status = get_sub_field('course_status');
	$link = get_sub_field('course_link');

	if ($status === 'available') {
		 $status_text = '<a href="' . $link . '" class="col-sm-5 course__status green">Ledige pladser</a>';
		 $status_color = 'green';
	}

	elseif ($status === 'few_available') {
		$status_text = '<a href="' . $link . '" class="col-sm-5 course__status yellow">Få ledige pladser</a>';
		$status_color = 'yellow';
	}

	elseif ($status === 'full') {
		$status_text = '<span class="col-sm-5 course__status red">Holdet er fyldt</span>';
		$status_color = 'red';
	}

	?>


	<div class="course__item flex flex--justify flex--v-center">
		<i class="far fa-calendar-alt col-sm-1 <?php echo esc_attr($status_color); ?>"></i>
		<span class="course__date col-sm-6"><?php echo esc_html($date); ?></span>
		 <?php echo $status_text; ?>
	</div>
	
<?php endwhile; endif; ?>