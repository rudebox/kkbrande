<footer class="footer gray--bg" id="footer">
	<div class="wrap hpad clearfix">
		<div class="row">
			<?php 
				if (have_rows('footer_columns', 'options') ) : while (have_rows('footer_columns', 'options') ) :
					the_row();
				$title = get_sub_field('column_title');
				$text = get_sub_field('column_text');
			 ?>

			 <div class="col-sm-4 footer__item">
			 	<h4 class="footer__title"><?php echo esc_html($title); ?></h4>
				<?php echo $text; ?>
			 </div>

			<?php endwhile; endif; ?>
		</div>
	</div>
</footer>


<?php wp_footer(); ?>

<?php if (is_front_page() ) : ?>
<script type="text/javascript" src="//cdn.curator.io/3.1/js/curator.js"></script>
<script type="text/javascript">
    // Change FEED_ID to your unique FEED_ID
    var widget = new Curator.Widgets.Carousel({
        debug:false,
        container:'#curator-feed',
        feedId:'bfaa6e93-00d1-40a1-9b2b-ad9395c66ce1',
        postClickAction:'none',

        carousel: {
        	infinite: true,
        	minWidth: 350
        }
    });
</script>
<?php endif; ?>

</body>
</html>
