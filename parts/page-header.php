<?php 
	if ( is_tax() ) {
		$term = get_term_by( 'slug', get_query_var( 'term' ), get_query_var( 'taxonomy' ) );
		$title = $term->name;
	} elseif ( is_category() ) {
		$title = single_cat_title( '', false );
	} elseif ( is_archive() ) {
		$title = post_type_archive_title( '', false );
	} elseif ( is_404() ) {
		$title = __('Siden blev ikke fundet', 'lionlab');
	} elseif ( is_search() ) {
		$title = __('Søgeresultat', 'lionlab');
	} else {
		$id = (is_home()) ? get_option('page_for_posts') : $post->ID;
		$title = get_proper_title($id);
	}

	//hero img with fallback
	$bg = get_field('page_img', $id) ?: get_field('page_img', 'options');

	$add_hero = get_field('add_hero');
?>

<?php if ($add_hero === true) : ?>
<section class="page__hero" style="background-image: url(<?php echo esc_html($bg['url']); ?> );">
	
</section>
<?php endif; ?>

<section class="page__header">
	<div class="wrap hpad">
		<h1 class="page__title"><?php echo esc_html($title); ?></h1>
	</div>
</section>