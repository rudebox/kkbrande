
<div class="course__wrapper gray--bg">

	<?php 
		//Course titles: Options page
		$title = get_field('course_title_car', 'options');
	 ?>

	<h2 class="course__title"><?php echo esc_html($title); ?></h2>

	<div class="course__content">

		<?php 

			//Course repeater field group: Options page
			$content = get_field('course_content_car', 'options');


			if (have_rows('course_car', 'options') ) : while (have_rows('course_car', 'options') ) : the_row();
			$date = get_sub_field('course_date_car');
			$status = get_sub_field('course_status_car');
			$link = get_sub_field('course_link_car');

			if ($status === 'available') {
				 $status_text = '<a href="' . $link . '" class="col-sm-5 course__status green">Ledige pladser</a>';
				 $status_color = 'green';
			}

			elseif ($status === 'few_available') {
				$status_text = '<a href="' . $link . '" class="col-sm-5 course__status yellow">Få ledige pladser</a>';
				$status_color = 'yellow';
			}

			elseif ($status === 'full') {
				$status_text = '<span class="col-sm-5 course__status red">Holdet er fyldt</span>';
				$status_color = 'red';
			}

		 ?>


		 <div class="course__item flex flex--justify flex--v-center">
		 	<i class="far fa-calendar-alt col-sm-1 <?php echo esc_attr($status_color); ?>"></i>
		 	<span class="course__date col-sm-6"><?php echo esc_html($date); ?></span>
		 	 <?php echo $status_text; ?>
		 </div>
		<?php endwhile; endif; ?>

		<div class="course__text">
		<?php echo _e($content); ?>
		</div>
	</div>
</div>
