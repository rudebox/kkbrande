<!doctype html>

<html <?php language_attributes(); ?> class="no-js">

<head>
  <meta charset="utf-8">
  <title><?php wp_title( '|', true, 'right' ); ?></title>
  <?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>

<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5N4PHBZ"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>

<?php 
  //contact indo
  $phone = get_field('phone', 'options'); 
  $mail = get_field('mail', 'options'); 
?>

<div class="toolbar gray--bg visible-xs">
  <div class="wrap hpad flex flex--end">
    <div class="toolbar__item">
      <a href="mailto:<?php echo esc_html($mail); ?>"><i class="fas fa-envelope"></i></a>
    </div>
    <div class="toolbar__item">
      <a href="tel:<?php echo get_formatted_phone($phone); ?>"><i class="fas fa-phone"></i></a>
    </div>
  </div>
</div>

<header class="header" id="header">
  <div class="header__toolbar">
  <div class="wrap hpad flex flex--center flex--justify">

    <a class="header__logo" href="<?php bloginfo('url'); ?>">
      <img src="<?php echo get_template_directory_uri(); ?>/assets/img/logo.png" alt="<?php bloginfo('name'); ?>">
    </a>

    <div class="header__toolbar--item flex flex--valign hidden-xs">
      <i class="fas fa-envelope"></i>
      <div class="header__toolbar--wrap flex">
        <strong>Send mig en mail</strong>
        <a href="mailto:<?php echo esc_html($mail); ?>"><?php echo esc_html($mail); ?></a>
      </div>
    </div>

    <div class="header__toolbar--item flex flex--valign hidden-xs">
      <i class="fas fa-phone"></i>
      <div class="header__toolbar--wrap flex">
        <strong>Ring til mig</strong>
        <a href="tel:<?php echo get_formatted_phone($phone); ?>"><?php echo esc_html($phone); ?></a>
      </div>
    </div>

    <div class="nav-toggle"> 
      <span class="nav-toggle__label"><?php _e('Menu', 'lionlab') ?></span> 
      <span class="nav-toggle__icon"></span>
    </div>
    </div>

  </div>
  

  <div class="header__nav">
    <div class="wrap hpad flex flex--center flex--end">
      <nav class="nav" role="navigation">
        <div class="nav--mobile">
          <?php scratch_main_nav(); ?>
        </div>
      </nav>
    </div>
  </div>

</header>
