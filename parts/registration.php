<?php 
/**
* Description: Lionlab parallax section field group layout
*
* @package Lionlab
* @subpackage Lionlab
* @since Version 1.0
* @author Kaspar Rudbech
*/

//sections settings
$bg = get_sub_field('bg');
$margin = get_sub_field('margin');
$title = get_sub_field('header');

//registration settings
$id = get_sub_field('registration_id');
?>

<section class="registration <?php echo esc_attr($bg); ?>--bg padding--<?php echo esc_attr($margin); ?>">
	<div class="wrap hpad">
		<?php if ($title) : ?>
		<h2 class="center"><?php echo esc_html($title); ?></h2>
		<?php endif; ?>
		<div class="row">

			<div class="col-sm-8 col-sm-offset-2 col-md-6 col-md-offset-3 registration__item">
				<?php  
					gravity_form( $id, $display_title = false, $display_description = false, $display_inactive = false, $field_values = null, $ajax = false, $echo = true );
				?>
			</div>

		</div>
	</div>
</section>