<?php 
/**
* Description: Lionlab parallax section field group layout
*
* @package Lionlab
* @subpackage Lionlab
* @since Version 1.0
* @author Kaspar Rudbech
*/

//sections settings
$bg = get_sub_field('bg');
$margin = get_sub_field('margin');
$title = get_sub_field('header');

//parallax settings
$text = get_sub_field('parallax_title');
$img = get_sub_field('parallax_bg');
?>

<section class="parallax <?php echo esc_attr($bg); ?>--bg padding--<?php echo esc_attr($margin); ?>" style="background-image: url(<?php echo esc_url($img['url']); ?>);">
	<div class="wrap hpad">
		<div class="row">

			<div class="col-sm-8 col-sm-offset-2 parallax__item">
				<h2 class="parallax__title h1"><?php echo esc_html($text); ?></h2>
			</div>

		</div>
	</div>
</section>