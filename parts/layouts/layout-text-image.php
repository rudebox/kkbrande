<?php

// Name of Flexible content clone field
$flex_id = 'text_image';

$class = get_sub_field( $flex_id . '_custom_class');
$position = get_sub_field( $flex_id . '_image_position');
$image = get_sub_field( $flex_id . '_image');
$content = get_sub_field( $flex_id . '_content');

$column_class = 'text-image__content entry ' . $position;
$column_class .= $position === 'left' ? ' col-sm-6 col-sm-offset-6' : ' col-sm-6';

//section settings
$margin = get_sub_field('margin');
$bg = get_sub_field('bg');
$heading = get_sub_field('header');
?>

<div class="text-image__wrapper">
	<section class="text-image padding--<?php echo esc_attr($margin); ?> <?php echo esc_attr($bg); ?>--bg">
		<div class="wrap hpad">
			<div class="row">

				<div class="<?= $column_class ?>">

					<?php
					$add_title = get_sub_field('add_title');
					if ( $add_title ) {
						echo get_section_title('title_', 'section__title');
					} ?>
					
					<h2 class="text-image__title"><?= $heading ?></h2>
					<?= $content ?>

				</div>

			</div>
		</div>
	</section>

	<div class="text-image__image <?= $position ?> <?php echo esc_attr($class); ?>" style="background-image: url(<?= $image['url']; ?>);"></div>

</div>
