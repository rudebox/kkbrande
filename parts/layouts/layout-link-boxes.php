<?php 
/**
* Description: Lionlab link-boxes repeater field group layout
*
* @package Lionlab
* @subpackage Lionlab
* @since Version 1.0
* @author Kaspar Rudbech
*/

//sections settings
$bg = get_sub_field('bg');
$margin = get_sub_field('margin');
$heading = get_sub_field('header');

if (have_rows('linkbox') ) :
?>

<section class="link-boxes <?php echo $bg; ?>--bg padding--<?php echo $margin; ?>">
	<div class="wrap hpad">
		<?php if ($heading) : ?>
		<h2 class="link-boxes__header"><?php echo esc_html($heading); ?></h2>
		<?php endif; ?>
		<div class="row flex flex--wrap">
			<?php while (have_rows('linkbox') ) : the_row(); 
				$title = get_sub_field('title');
				$text = get_sub_field('text');
				$img = get_sub_field('icon');
				$link = get_sub_field('link');
			?>

			<a data-aos="fade-up" href="<?php echo esc_url($link); ?>" class="col-sm-3 link-boxes__item">
				<?php if ($img) : ?>
				<div class="link-boxes__wrap">
					<div class="link-boxes__overlay"></div>
					<h3 class="link-boxes__title link-boxes__title--hover"><?php echo esc_html($title); ?></h3>
					<img class="link-boxes__img" src="<?php echo esc_url($img['sizes']['link-boxes']); 
					?>" alt="<?php echo esc_attr($img['alt']); ?>">
				</div>
				<?php endif; ?>
				<h3 class="link-boxes__title"><?php echo esc_html($title); ?></h3>
				<?php echo $text; ?>
			</a>
			<?php endwhile; ?>
		</div>
	</div>
</section>
<?php endif; ?>