<?php

/*
 * Template Name: Contact
 */

get_template_part('parts/header'); the_post(); ?>

<main>
	
	<?php get_template_part('parts/page', 'header');?>

	<section class="padding--bottom">
		<div class="wrap hpad">
			<div class="row">

				<?php 
      				//contact indo
      				$phone = get_field('phone', 'options'); 
      				$mail = get_field('mail', 'options'); 
      				$adress = get_field('address', 'options');
      				$cvr = get_field('cvr', 'options'); 
    			?>

				<div class="col-sm-6 contact__info">
					<?php echo $adress; ?>

					Telefon: <a href="tel:<?php echo get_formatted_phone($phone); ?>"><?php echo esc_html($phone); ?></a><br>
					E-mail: <a href="mailto:<?php echo esc_html($mail); ?>"><?php echo esc_html($mail); ?></a><br>

					CVR: <?php echo esc_html($cvr); ?>
				</div>

				<div class="col-sm-6 contact__form">
					<?php  
						gravity_form( 2, $display_title = false, $display_description = false, $display_inactive = false, $field_values = null, $ajax = false, $echo = true );
					?>
				</div>

			</div>
		</div>
	</section>

	<?php echo get_template_part('parts/google', 'maps'); ?>

</main>

<?php get_template_part('parts/footer'); ?>
