<?php

/* Do not remove this line. */
require_once('includes/scratch.php');
require_once('lib/theme-dependencies.php');


/*
 * adds all meta information to the <head> element for us.
 */

function scratch_meta() { ?>

  <meta http-equiv="X-UA-Compatible" content="IE=edge, chrome=1">
  <meta name="description" content="<?php bloginfo('description'); ?>">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="format-detection" content="telephone=no">

  <link rel="apple-touch-icon" href="/apple-touch-icon.png">
  <!-- Place favicon.ico in the root directory -->

  <!-- Google Tag Manager -->
  <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
  new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
  j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
  'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
  })(window,document,'script','dataLayer','GTM-5N4PHBZ');</script>

  <!-- Cookies -->
  <link rel="stylesheet" type="text/css" href="//cdnjs.cloudflare.com/ajax/libs/cookieconsent2/3.1.0/cookieconsent.min.css" />
  <script src="//cdnjs.cloudflare.com/ajax/libs/cookieconsent2/3.1.0/cookieconsent.min.js"></script>
  <script>
  window.addEventListener("load", function(){
  window.cookieconsent.initialise({
    "palette": {
      "popup": {
        "background": "#ffffff",
        "text": "#33383b"
      },
      "button": {
        "background": "#33383b"
      }
    },
    "theme": "classic",
    "position": "bottom-right",
    "content": {
      "message": "Denne hjemmeside bruger cookies for at sikre, at du får den bedste oplevelse på vores hjemmeside.",
      "dismiss": "OK",
      "link": "Læs mere",
      "href": "/fortrolighedspolitik"
    }
  })});
  </script>

<?php }

add_action('wp_head', 'scratch_meta');

/* Theme CSS */

function theme_styles() {

  wp_enqueue_style( 'normalize', 'https://cdnjs.cloudflare.com/ajax/libs/normalize/8.0.0/normalize.min.css', false, null );

  wp_enqueue_style( 'curator', '//cdn.curator.io/3.1/css/curator.css', false, null );

  wp_enqueue_style( 'aos', 'https://cdnjs.cloudflare.com/ajax/libs/aos/2.3.4/aos.css', false, null );

  wp_enqueue_style( 'fontawesome', 'https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.8.1/css/all.css', false, null );

  wp_register_style( 'scratch-main', get_template_directory_uri() . '/assets/css/master.css', false, filemtime(dirname(__FILE__) . '/assets/css/master.css') );
  wp_enqueue_style( 'scratch-main' );

}

add_action( 'wp_enqueue_scripts', 'theme_styles' );

/* Theme JavaScript */

function theme_js() {

  wp_enqueue_script( 'aos', 'https://cdnjs.cloudflare.com/ajax/libs/aos/2.3.4/aos.js', false, false, false );

  wp_register_script( 'scratch-main-concat', get_template_directory_uri() . '/assets/js/concat/main.js', array('jquery'), filemtime(dirname(__FILE__) . '/assets/js/concat/main.js'), true ); 

  wp_register_script( 'scratch-main-min', get_template_directory_uri() . '/assets/js/build/main.min.js', array('jquery'), filemtime(dirname(__FILE__) . '/assets/js/build/main.min.js'), true );


  /* FOR DEVELOPMENT */
  // wp_enqueue_script( 'scratch-main-concat' );

  /* FOR PRODUCTION */
  wp_enqueue_script( 'scratch-main-min' );

}

add_action( 'wp_enqueue_scripts', 'theme_js' );


/* Enable ACF Options Pages */


if( function_exists('acf_add_options_page') ) {
  
  acf_add_options_page(array(
    'page_title'  => 'Globalt indhold',
    'menu_title'  => 'Globalt indhold',
    'menu_slug'   => 'global-content',
    'capability'  => 'edit_posts',
    'redirect'    => false
  ));
  
  acf_add_options_sub_page(array(
    'page_title'  => 'Header',
    'menu_title'  => 'Header',
    'parent_slug' => 'global-content',
  ));

  acf_add_options_sub_page(array(
    'page_title'  => 'Sidebar',
    'menu_title'  => 'Sidebar',
    'parent_slug' => 'global-content',
  ));
  
  acf_add_options_sub_page(array(
    'page_title'  => 'Footer',
    'menu_title'  => 'Footer',
    'parent_slug' => 'global-content',
  ));
}

/* Enable Featured Image */

add_theme_support( 'post-thumbnails' );

//custom image sizes
add_image_size('link-boxes', 800, 486, true);

// Add created images sizes to dropdown in WP control panel
add_filter( 'image_size_names_choose', 'custom_image_sizes' );

function custom_image_sizes( $sizes ) {
  return array_merge( $sizes, array(
    'link-boxes' => __( 'link-boxes' ),
  ) );
}  

/* Enable Custom Menus */

add_theme_support( 'menus' );

register_nav_menus(
  array(
    'scratch-main-nav' => __( 'Main Nav', 'scratch' )   // main nav in header
  )
);

function scratch_main_nav() {
  // display the wp3 menu if available
  wp_nav_menu(array(
    'container' => false, // remove nav container
    'container_class' => '', // class of container (should you choose to use it)
    'menu' => __( 'Main Nav', 'scratch' ), // nav name
    'menu_class' => 'nav__menu', // adding custom nav class
    'theme_location' => 'scratch-main-nav', // where it's located in the theme
    'before' => '', // before the menu
    'after' => '', // after the menu
    'link_before' => '', // before each link
    'link_after' => '', // after each link
    'depth' => 0,    // fallback function
    'walker' => new Clean_Walker_Nav () //remove wordpress walker
  ));
} /* end scratch main nav */


// Create Shortcode course_status
// Use the shortcode: [course_status]
function create_coursestatus_shortcode() {
  ob_start(); 

    get_template_part('parts/course', 'status');

  return ob_get_clean();
}

add_shortcode( 'course_status', 'create_coursestatus_shortcode' );

// Create Shortcode course_dates
// Use the shortcode: [course_dates]
function create_coursedates_shortcode() {
  ob_start(); 

    get_template_part('parts/course', 'dates');

  return ob_get_clean();
}

add_shortcode( 'course_dates', 'create_coursedates_shortcode' );



// Create Shortcode course_status_car
// Use the shortcode: [course_status_car]
function create_coursestatus_car_shortcode() {
  ob_start(); 

    get_template_part('parts/course', 'status-car');

  return ob_get_clean();
}

add_shortcode( 'course_status_car', 'create_coursestatus_car_shortcode' );

// Create Shortcode course_dates_car
// Use the shortcode: [course_dates_car]
function create_coursedates_car_shortcode() {
  ob_start(); 

    get_template_part('parts/course', 'dates-car');

  return ob_get_clean();
}

add_shortcode( 'course_dates_car', 'create_coursedates_car_shortcode' );


// Create Shortcode course_status_trailer
// Use the shortcode: [course_status_trailer]
function create_coursestatus_trailer_shortcode() {
  ob_start(); 

    get_template_part('parts/course', 'status-trailer');

  return ob_get_clean();
}

add_shortcode( 'course_status_trailer', 'create_coursestatus_trailer_shortcode' );

// Create Shortcode course_dates_trailer
// Use the shortcode: [course_dates_trailer]
function create_coursedates_trailer_shortcode() {
  ob_start(); 

    get_template_part('parts/course', 'dates-trailer');

  return ob_get_clean(); 
}

add_shortcode( 'course_dates_trailer', 'create_coursedates_trailer_shortcode' );

//scroll to anchor on GF sumbit
add_filter( 'gform_confirmation_anchor', '__return_true' );


/* Place custom functions below here. */

/* Don't delete this closing tag. */
?>
